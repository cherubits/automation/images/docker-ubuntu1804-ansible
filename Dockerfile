FROM ubuntu:18.04
LABEL maintainer="László Hegedűs"

ENV pip_packages "pip==9.0.3 setuptools-rust"
ENV PYTHONIOENCODING UTF-8
ENV DEBIAN_FRONTEND noninteractive

# Install dependencies.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       apt-utils \
       locales \
       software-properties-common \
       rsyslog systemd systemd-cron sudo iproute2 \
    && rm -Rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean
RUN sed -i 's/^\($ModLoad imklog\)/#\1/' /etc/rsyslog.conf

# Fix potential UTF-8 errors with ansible-test.
RUN locale-gen en_US.UTF-8

RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt-get update


RUN apt-get install -y --no-install-recommends \
    python3.8 \
    python3-pip

RUN ln -s /usr/bin/python3.8 /usr/local/bin/python 
#RUN ln -s /usr/bin/pip3 /usr/local/bin/pip



RUN python -m pip install -U pip setuptools setuptools-rust
RUN echo "Python: $(python --version)\n" && echo "Pip: $(pip3 --version)\n"

# Install Ansible via Pip.
#RUN pip install $pip_packages

RUN pip3 install \
  ansible==2.10.7 \
  PyYAML==5.4.1

COPY initctl_faker .
RUN chmod +x initctl_faker && rm -fr /sbin/initctl && ln -s /initctl_faker /sbin/initctl

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts



# Remove unnecessary getty and udev targets that result in high CPU usage when using
# multiple containers with Molecule (https://github.com/ansible/molecule/issues/1104)
RUN rm -f /lib/systemd/system/systemd*udev* \
  && rm -f /lib/systemd/system/getty.target

VOLUME ["/sys/fs/cgroup", "/tmp", "/run"]
CMD ["/lib/systemd/systemd"]
